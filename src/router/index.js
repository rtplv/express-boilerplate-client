import Vue from 'vue';
import Router from 'vue-router';

import Register from '@/views/Register';
import SignIn from '@/views/SignIn';
import Posts from '@/views/Posts';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Posts',
      component: Posts,
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
    },
    {
      path: '/signin',
      name: 'signin',
      component: SignIn,
    },
  ],
});
