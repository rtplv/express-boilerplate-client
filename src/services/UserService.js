import api from '@/services/api';

export default {
  register(user) {
    return api().post('users/register', user);
  },
  signIn(user) {
    return api().post('users/signin', user);
  },
};
