import axios from 'axios';
// import notifications from '../utils/notifications';

export default () => {
  const params = {
    baseURL: 'http://localhost:8081',
  };
  const accessToken = localStorage.getItem('accessToken');

  if (accessToken) {
    params.headers = {
      Authorization: `JWT ${accessToken}`,
    };
  }
  const instance = axios.create(params);
  // // Обрабатываем 401 ошибку, и редиректим юзера на страницу входа
  instance.interceptors.response.use(null, async err => {
    // notifications.error(err.response.data.error);
    if (err.response.status === 401) {
      console.log(err);
      if(err.response.tokenExpired){
        const res = await instance.post('users/refreshToken', {
          refreshToken: localStorage.getItem('refreshToken')
        });
        console.log(res);
      }
      // window.location.href = '/signin';
    }
  });

  return instance;
};
