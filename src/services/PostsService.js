import api from '@/services/api';

export default {
  fetchPosts() {
    return api().get('posts/get');
  },
  getPostById(id) {
    return api().get(`posts/get/${id}`);
  },
  createPost(post) {
    return api().post('posts/create', post);
  },
  updatePost(post) {
    const { title, description, _id } = post;
    return api().put(`posts/update/${_id}`, { title, description });
  },
  deletePost(id) {
    return api().delete(`posts/delete/${id}`);
  },
};
